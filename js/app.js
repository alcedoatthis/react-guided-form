"use strict";

var _createClass = (function () { function defineProperties(target, props) { for (var key in props) { var prop = props[key]; prop.configurable = true; if (prop.value) prop.writable = true; } Object.defineProperties(target, props); } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

var _classCallCheck = function (instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } };

var Utils = {};

(function (window, document, Utils) {
	var Validator = (function () {
		function Validator(rules) {
			_classCallCheck(this, Validator);

			this.rules = rules;
		}

		_createClass(Validator, {
			validate: {
				value: function validate(data) {}
			}
		});

		return Validator;
	})();

	Utils.Validator = Validator;
})(window, document, Utils);

(function (window, document, Utils) {
	var Router = (function () {
		function Router(mapping) {
			_classCallCheck(this, Router);

			this.mapping = mapping;
			this.getState();
		}

		_createClass(Router, {
			getState: {
				value: function getState() {
					var index, hash;
					hash = document.location.hash.replace(/^#+/, "") || null;
					index = this.mapping.indexOf(hash);

					this.state = index > -1 ? index : 0;
					console.info("Router got hash " + document.location.hash + " (i~" + index + ")");
				}
			},
			setState: {
				value: function setState(index) {
					document.location.hash = this.mapping[index];
					this.state = index;
					console.info("Router setting hash to " + document.location.hash + " (i~" + index + ")");
				}
			}
		});

		return Router;
	})();

	Utils.Router = Router;
})(window, document, Utils);

var Input = React.createClass({ displayName: "Input",
	getInitialState: function getInitialState() {
		return { value: this.props.value, validations: this.props.validations };
	},
	handleChange: function handleChange(e) {
		this.setState({ value: e.target.value });
		this.props.changeCb(this.props.idKey, e.target.value, this.state.validations);
	},
	render: function render() {
		return React.createElement("div", { className: "form-group input" }, React.createElement("label", { htmlFor: this.props.idKey }, this.props.label), React.createElement("input", { type: this.props.type, id: this.props.idKey, value: this.state.value, onChange: this.handleChange, className: "form-control" }));
	}
});

var Chbox = React.createClass({ displayName: "Chbox",
	getInitialState: function getInitialState() {
		return { checked: this.props.checked, validations: this.props.validations };
	},
	handleChange: function handleChange(e) {
		this.setState({ checked: e.target.checked });
		this.props.changeCb(this.props.idKey, e.target.checked, this.state.validations);
	},
	render: function render() {
		return React.createElement("div", { className: "form-group checkbox" }, React.createElement("input", { type: "checkbox", id: this.props.idKey, checked: this.state.checked, onChange: this.handleChange }), React.createElement("label", { htmlFor: this.props.idKey }, this.props.label));
	}
});

var Fields = React.createClass({ displayName: "Fields",
	makeInput: function makeInput(field, key, userInput) {
		return React.createElement(Input, { type: field.type,
			label: field.label,
			validations: field.validations,
			idKey: key,
			key: key,
			changeCb: this.props.dataCollector,
			value: userInput });
	},
	makeChbox: function makeChbox(field, key, userInput) {
		return React.createElement(Chbox, { label: field.label,
			validations: field.validations,
			idKey: key,
			key: key,
			changeCb: this.props.dataCollector,
			checked: userInput });
	},
	render: function render() {
		var fieldNodes = [],
		    kind,
		    fKey,
		    userInput;
		for (var fKey in this.props.list) {
			kind = this.props.list[fKey].type === "checkbox" ? "Chbox" : "Input";
			userInput = fKey in this.props.screenData ? this.props.screenData[fKey] : null;
			userInput = userInput !== null && userInput.hasOwnProperty("val") ? userInput.val : null;
			fieldNodes.push(this["make" + kind](this.props.list[fKey], fKey, userInput)); // TODO (0?)
		}
		return React.createElement("div", { className: "fields" }, fieldNodes);
	}
});

var Controls = React.createClass({ displayName: "Controls",
	// disabler: function() {
	// 	React.findDOMNode(this.refs.backBtn).disabled = this.props.backDisabled;
	// },
	handleStateChange: function handleStateChange(e) {
		if (e.target.classList.contains("submit")) {
			this.props.changeStep(1);
		} else {
			this.props.changeStep(-1);
		}
	},
	// componentDidMount: function() {
	// 	this.disabler();
	// },
	render: function render() {
		return React.createElement("div", { className: "controls" }, React.createElement("button", { onClick: this.handleStateChange, className: "submit btn btn-success pull-right" }, this.props.submitLabel), React.createElement("button", { onClick: this.handleStateChange, className: "btn btn-warning", ref: "backBtn", disabled: this.props.backDisabled }, "Back"));
	}
});

var StepsForm = React.createClass({ displayName: "StepsForm",
	getInitialState: function getInitialState() {
		var step;
		this.mapping = [];
		this.collectedData = [];
		this.validator = new Utils.Validator(rules);
		for (step in recipe.registration.steps) {
			this.mapping.push(step);
			// prazdny objekt pro data kazde screeny
			this.collectedData.push({});
		}
		this.router = new Utils.Router(this.mapping);
		return {
			steps: recipe.registration.steps,
			current: {
				step: recipe.registration.steps[this.mapping[this.router.state]],
				index: this.router.state
			}
		};
	},
	move: function move(pos) {
		var next = Math.max(0, this.state.current.index + pos),
		    max = this.mapping.length - 1;
		if (next > max) {
			console.warn("End of form, validating ...");

			return;
		}
		this.setState({ current: {
				index: next,
				step: this.state.steps[this.mapping[next]]
			} }, this.router.setState.bind(this.router, next));
	},
	dataCollector: function dataCollector(key, val, validations) {
		this.collectedData[this.state.current.index][key] = { key: key, val: val, validations: validations, step: this.state.current.index };
		console.log(JSON.stringify(this.collectedData));
	},
	render: function render() {
		return React.createElement("div", { className: "StepsForm" }, React.createElement("h2", null, this.state.current.step.title), React.createElement(Fields, { list: this.state.current.step.fields,
			dataCollector: this.dataCollector,
			screenData: this.collectedData[this.state.current.index] }), React.createElement(Controls, { backDisabled: this.state.current.index === 0,
			completed: this.state.current.index + 1 === this.mapping.length,
			submitLabel: this.state.current.step.submit,
			changeStep: this.move }));
	}
});

React.render(React.createElement(StepsForm, null), document.getElementsByTagName("body")[0]);