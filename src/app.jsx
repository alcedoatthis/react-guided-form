var Utils = {};

(function(window, document, Utils) {
	class Validator {
		constructor(rules) {
			this.rules = rules;
		}
		validate(data) {

		}
	}
	Utils.Validator = Validator;
})(window, document, Utils);

(function(window, document, Utils) {
	class Router {
		constructor(mapping){
			this.mapping = mapping;
			this.getState();
		}
		getState(){
			var index, hash;
			hash = document.location.hash.replace(/^#+/, "") || null;
			index = this.mapping.indexOf(hash);

			this.state = index > -1 ? index : 0;
			console.info("Router got hash "+document.location.hash+" (i~"+index+")");
		}
		setState(index){
			document.location.hash = this.mapping[index];
			this.state = index;
			console.info("Router setting hash to "+document.location.hash+" (i~"+index+")");
		}

	}
	Utils.Router = Router;
})(window, document, Utils);



var Input = React.createClass({
	getInitialState: function() {
		return {value: this.props.value, validations: this.props.validations};
	},
	handleChange: function(e) {
		this.setState({value: e.target.value});
		this.props.changeCb(this.props.idKey, e.target.value, this.state.validations);
	},
	render: function() {
		return (
		    <div className="form-group input">
			    <label htmlFor={this.props.idKey}>
			    	{this.props.label}
			    </label>
				<input type={this.props.type} id={this.props.idKey} value={this.state.value} onChange={this.handleChange} className="form-control" />
			</div>
		);
	}
});

var Chbox = React.createClass({
	getInitialState: function() {
		return {checked: this.props.checked, validations: this.props.validations};
	},
	handleChange: function(e) {
		this.setState({checked: e.target.checked});
		this.props.changeCb(this.props.idKey, e.target.checked, this.state.validations);
	},
	render: function() {
		return (
		    <div className="form-group checkbox">
				<input type="checkbox" id={this.props.idKey} checked={this.state.checked} onChange={this.handleChange} />
			    <label htmlFor={this.props.idKey}>
			    	{this.props.label}
			    </label>
			</div>
		);
	}
});

var Fields = React.createClass({
	makeInput: function(field, key, userInput) {
		return (
			<Input 	type={field.type} 
					label={field.label} 
					validations={field.validations}
					idKey={key}
					key={key}
					changeCb={this.props.dataCollector}
					value={userInput}
			/>
		);
	},
	makeChbox: function(field, key, userInput) {
		return (
			<Chbox 	label={field.label} 
					validations={field.validations}
					idKey={key}
					key={key}
					changeCb={this.props.dataCollector}
					checked={userInput}
			/>
		);
	},
	render: function () {
		var fieldNodes = [], 
			kind,
			fKey,
			userInput;
		for(var fKey in this.props.list) {
			kind = this.props.list[fKey].type === "checkbox" ? "Chbox" : "Input";
			userInput = (fKey in this.props.screenData) ? 
				this.props.screenData[fKey] : null;
			userInput = (userInput !== null && userInput.hasOwnProperty("val")) ?
				userInput.val : null;
			fieldNodes.push(this["make"+kind](this.props.list[fKey], fKey, userInput)); // TODO (0?)
		}
		return (
			<div className="fields">
				{fieldNodes}
			</div>
	    );
	}
});

var Controls = React.createClass({
	// disabler: function() {
	// 	React.findDOMNode(this.refs.backBtn).disabled = this.props.backDisabled;
	// },
	handleStateChange: function(e) {
		if (e.target.classList.contains("submit")) {
			this.props.changeStep(1);
		} else {
			this.props.changeStep(-1);
		}
	},
	// componentDidMount: function() {
	// 	this.disabler();
	// },
	render: function() {
		return (
			<div className="controls">
				<button onClick={this.handleStateChange} className="submit btn btn-success pull-right">
					{this.props.submitLabel}
				</button>
				<button onClick={this.handleStateChange} className="btn btn-warning" ref="backBtn" disabled={this.props.backDisabled} >
					Back
				</button>
			</div>
		);
	}
});

var StepsForm = React.createClass({
	getInitialState: function () {
		var step;
		this.mapping = [];
		this.collectedData = [];
		this.validator = new Utils.Validator(rules);
		for (step in recipe.registration.steps) {
			this.mapping.push(step);
			// prazdny objekt pro data kazde screeny
			this.collectedData.push({});
		}
		this.router = new Utils.Router(this.mapping);
		return {
			steps: recipe.registration.steps,
			current: {
				step: recipe.registration.steps[this.mapping[this.router.state]],
				index: this.router.state
			}
		};
	},
	move: function(pos) {
		var next = Math.max(0, this.state.current.index+pos),
			max = this.mapping.length-1;
		if (next > max) {
			console.warn("End of form, validating ...");

			return;
		}
		this.setState({current: {
			index: next,
			step: this.state.steps[this.mapping[next]]
		}}, this.router.setState.bind(this.router, next));
	},
	dataCollector: function(key,val,validations) {
		this.collectedData[this.state.current.index][key] = {key: key, val: val, validations: validations, step: this.state.current.index};
		console.log(JSON.stringify(this.collectedData));
	},
	render: function() {
		return (
			<div className="StepsForm">
				<h2>{this.state.current.step.title}</h2>
				<Fields list={this.state.current.step.fields} 
						dataCollector={this.dataCollector}
						screenData={this.collectedData[this.state.current.index]} />
				<Controls backDisabled={this.state.current.index === 0} 
						completed={this.state.current.index+1 === this.mapping.length}
						submitLabel={this.state.current.step.submit}
						changeStep={this.move} />
			</div>
		);
	}
});

React.render(
	<StepsForm />,
	document.getElementsByTagName('body')[0]
);
