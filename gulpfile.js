var gulp = require("gulp");
var react = require('gulp-react');
var babel = require("gulp-babel");
var browserSync = require('browser-sync');

// transpilace es6
gulp.task("transpiler", function () {
  return gulp.src("src/app.jsx")
  	.pipe(react())
    .pipe(babel())
    .pipe(gulp.dest("js"));
});

// browsersync
gulp.task('js-watch', ['transpiler'], browserSync.reload);
gulp.task('default', ['transpiler'], function () {
    browserSync({
        server: {
            baseDir: "./"
        }
    });
    gulp.watch(["src/*.js*", "./*.html"], ['js-watch']);
});
