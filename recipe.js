var recipe = {
  "$pendingActions": {},
  "i18n": {
    "formats": {},
    "locale": "en",
    "messages": {
      "en": {
        "registration": {
          "saveAndContinue": "Save and continue"
        }
      }
    }
  },
  "registration": {
    "steps": {
      "__default": {
        "title": "Get started",
        "description": "To apply, you will need: ...",
        "submit": "Create your account",
        "fields": {
          "email": {
            "type": "email",
            "label": "Email",
            "validations": [
              "required",
              "email"
            ]
          },
          "password": {
            "type": "password",
            "label": "Choose a password",
            "hint": "NOTE: must be at least 6 characters long & include 1 number",
            "validations": [
              "required",
              "password"
            ]
          },
          "acceptAgreement": {
            "type": "checkbox",
            "label": "I confirm that I understand the terms & conditions & agree to the privacy policy",
            "validations": [
              "required"
            ]
          },
          "agreeGetNews": {
            "type": "checkbox",
            "label": "Can we get in touch with you when we have offers & deals on?"
          }
        }
      },
      "personal-details": {
        "title": "Personal details",
        "description": "We will not share personal information with marketeers or spammers.",
        "submit": "Save & continue",
        "fields": {
          "personalId": {
            "type": "text",
            "label": "CPR number",
            "hint": "We use this to help verify your identity & gather information for your credit check",
            "validations": [
              "required",
              "cpr"
            ]
          },
          "phone": {
            "type": "text",
            "label": "Phone number",
            "hint": "Either your mobile or home phone if you dont have a mobile.",
            "validations": [
              "required",
              "phone"
            ]
          }
        }
      },
      "your-finances": {
        "title": "Your finances",
        "description": "As responsible lenders we need to understand how much you can afford pay each month",
        "submit": "Save & continue",
        "fields": {
          "incomeAmount": {
            "type": "number",
            "label": "Your average monthly income",
            "hint": "After tax",
            "validations": [
              "required",
              "number"
            ]
          },
          "housingExpense": {
            "type": "number",
            "label": "Monthly rent/mortgage cost",
            "validations": [
              "required",
              "number"
            ]
          },
          "otherExpense": {
            "type": "number",
            "label": "Average monthly bills",
            "hint": "Utilities & any large expenses like car loans, etc",
            "validations": [
              "required",
              "number"
            ]
          }
        }
      },
      "enter-bank-account": {
        "title": "Where do you want the money?",
        "description": "This is where we will deliver the money to, once you confirm your loan.",
        "submit": "Save & continue for decision",
        "fields": {
          "accountNumber": {
            "type": "text",
            "label": "Bank account number",
            "hint": "We use bank level security",
            "validations": [
              "required",
              "account_number"
            ]
          }
        }
      }
    }
  }
};