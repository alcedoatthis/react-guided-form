var rules = {
	email: {
		minLength: 5,
		match: "^[^\\s]+@[^\\s]+\\.\\w+"
	}
};